#include "tiny1wireX4_INT0.h"

uint8_t owState; // current state
uint8_t owBitCount; // bit count down counter
uint8_t owByteCount; // byte count down counter
uint8_t owByte; // in- or out-bound byte
uint8_t owBuf[8]; // buffer for ROM code or up to 8 bytes of data
uint8_t* owBufPtr; // pointer to current buffer byte
uint8_t owCRC;
bool resumeFlag;

uint8_t owRomCode[8];
void (*callback)(uint8_t state, uint8_t* buf);

void owSetup(const uint8_t code[7], void (*callback_ptr)(uint8_t state, uint8_t* buf))
{
    callback = callback_ptr;

    memcpy(owRomCode, code, 7);
    owRomCode[7] = crc8(owRomCode, 7); // Update owRomCode CRC

    owState = IDLE;
    OW_RELEASE;

    BIT_SET(MCUCR, ISC01); // The falling edge of INT0 generates an interrupt request
    BIT_SET(GIMSK, INT0); // Enable INT0

    DB_BIT_SET(PORTA, 2);
}

void owTick() // one execution per reset or bit
{
    DB_PULSE(PORTA, 0);
    // if (OW_IS_HIGH)
    if (TCNT0 == 0)
        return;

    cli();

    if (owState & RX) {
        delayMicroseconds(25); // wait before testing line
        DB_PULSE(PORTA, 1);
        owByte >>= 1;
        if (OW_IS_HIGH) {
            DB_PULSE(PORTA, 1);
            owByte |= 0x80;
        }
        owBitCount -= 1;
        if (owBitCount == 0) {
            DB_DUMP_TO_PIN(owByte, 1);
            owByteRecd();
        }
        while (OW_IS_LOW) { // wait here for line to be released
            if (TCNT0 > TCERROR) { // should not have taken this long - something is wrong
                owState = IDLE;
                break;
            }
        }
    } else if (owState & TX) {
        // DB_PULSE(PORTA, DB_PIN2);
        if ((owByte & 0x01) == 0) {
            OW_ASSERT;
            delayMicroseconds(45);
            OW_RELEASE;
        }
        owByte >>= 1;
        owBitCount -= 1;
        if (owBitCount == 0) {
            owByteCount -= 1;
            if (owByteCount == 0) {
                owRxBytes(1, RXDEVCMD); // After sending the last byte assume the master still wants to talk to us so prepare to receive the next command.
            } else {
                owBufPtr += 1;
                owByte = *owBufPtr;
                owBitCount = 8;
            }
        }
        while (OW_IS_LOW) { // wait here for line to be released
            if (TCNT0 > TCERROR) { // should not have taken this long - something is wrong
                owState = IDLE;
                break;
            }
        }
    }

    if (owState == IDLE) {
        DB_BIT_SET(PORTA, 1);
        while (OW_IS_LOW) { // wait here for line to be released
            if (BIT_READ(TIFR0, TOV0)) { // timer overflow?
                DB_BIT_CLEAR(PORTA, 1);
                DB_BIT_SET(PORTA, 1);
                TCCR0B = 0; // stop timer
                TCNT0 = 0xFF; // set timer to MAX
                BIT_SET(TIFR0, TOV0); // Timer/Counter0 Overflow Flag - set to 1 to clear;
            }
        }
        if (TCNT0 == TCRESET) { // this was a reset pulse
            delayMicroseconds(20);
            OW_ASSERT; // let the master know we are here
            delayMicroseconds(80);
            OW_RELEASE;
            owRxBytes(1, RXROMCMD); // expect the next byte to be a ROM command
        }
        DB_BIT_CLEAR(PORTA, 1);
        DB_BIT_SET(PORTA, 2);
    }
    TCCR0B = 0; // stop timer
    TCNT0 = 0; // reset timer

    BIT_SET(GIFR, INTF0); // Clear the INT0 flag in case it was set by us fiddling with the 1wire pin
    sei();
}

void owTxBytes(uint8_t* buf, uint8_t len, uint8_t newState)
{
    owByteCount = len;
    owBitCount = 8;
    owBufPtr = buf;
    owByte = *owBufPtr;
    owState = newState;
}

void owRxBytes(uint8_t len, uint8_t newState)
{
    owByteCount = len;
    owBitCount = 8;
    owByte = 0;
    owBufPtr = owBuf;
    owState = newState;
    owCRC = 0;
}

void owByteRecd()
{
    if (owState == RXROMCMD) {
        switch(owByte) {
        case OW_ROM_READ:
            owTxBytes(owRomCode, 8, TXROMCODE);
            resumeFlag = false;
            break;
        case OW_ROM_MATCH:
            owRxBytes(8, RXROMCODE);
            resumeFlag = false;
            break;
        case OW_ROM_SKIP:
            owRxBytes(1, RXDEVCMD);
            resumeFlag = false;
            break;
        case OW_ROM_RESUME:
            if (resumeFlag)
                owRxBytes(1, RXDEVCMD);
            else
                owState = IDLE;
        }
    } else if (owState == RXDEVCMD) {
        callback(owState, &owByte);
    } else { // All states where we are receiving multiple bytes
        *owBufPtr = owByte;
        owCRC = crc8(owByte, owCRC);
        owByteCount -= 1;
        if (owByteCount > 0) { // more bytes to receive
            owBitCount = 8; // reset the bit count
            owByte = 0; // and owByte to prepare to receive the next byte
            owBufPtr += 1;
        } else {
            if (owCRC == 0) { // Valid data received
                if (owState == RXROMCODE) {
                    if (memcmp(owBuf, owRomCode, 8) == 0) { // Did we receive our ROM code?
                        owRxBytes(1, RXDEVCMD); // If so, expect the next byte to be a command.
                        resumeFlag = true;
                    } else {
                        owState = IDLE; // If not, go back to idle.
                    }
                } else {
                    callback(owState, owBuf);
                }
            } else { // Data received is not valid
                owState = IDLE; // Go back to idle.
            }
        }
    }
}

void owAssert()
{
    OW_DDRX |= OW_MASK; // BIT_CLEAR(PORTB, 1) // set to output - should already be low
}

void owRelease()
{
    OW_DDRX &= ~OW_MASK; // BIT_SET(PORTB, 1) // set to input to tri-state and allow external pull-up to return line to high
}

const uint8_t PROGMEM CRC8_table[256] = {
    0x00, 0x5E, 0xBC, 0xE2, 0x61, 0x3F, 0xDD, 0x83, 0xC2, 0x9C, 0x7E, 0x20, 0xA3, 0xFD, 0x1F, 0x41,
    0x9D, 0xC3, 0x21, 0x7F, 0xFC, 0xA2, 0x40, 0x1E, 0x5F, 0x01, 0xE3, 0xBD, 0x3E, 0x60, 0x82, 0xDC,
    0x23, 0x7D, 0x9F, 0xC1, 0x42, 0x1C, 0xFE, 0xA0, 0xE1, 0xBF, 0x5D, 0x03, 0x80, 0xDE, 0x3C, 0x62,
    0xBE, 0xE0, 0x02, 0x5C, 0xDF, 0x81, 0x63, 0x3D, 0x7C, 0x22, 0xC0, 0x9E, 0x1D, 0x43, 0xA1, 0xFF,
    0x46, 0x18, 0xFA, 0xA4, 0x27, 0x79, 0x9B, 0xC5, 0x84, 0xDA, 0x38, 0x66, 0xE5, 0xBB, 0x59, 0x07,
    0xDB, 0x85, 0x67, 0x39, 0xBA, 0xE4, 0x06, 0x58, 0x19, 0x47, 0xA5, 0xFB, 0x78, 0x26, 0xC4, 0x9A,
    0x65, 0x3B, 0xD9, 0x87, 0x04, 0x5A, 0xB8, 0xE6, 0xA7, 0xF9, 0x1B, 0x45, 0xC6, 0x98, 0x7A, 0x24,
    0xF8, 0xA6, 0x44, 0x1A, 0x99, 0xC7, 0x25, 0x7B, 0x3A, 0x64, 0x86, 0xD8, 0x5B, 0x05, 0xE7, 0xB9,
    0x8C, 0xD2, 0x30, 0x6E, 0xED, 0xB3, 0x51, 0x0F, 0x4E, 0x10, 0xF2, 0xAC, 0x2F, 0x71, 0x93, 0xCD,
    0x11, 0x4F, 0xAD, 0xF3, 0x70, 0x2E, 0xCC, 0x92, 0xD3, 0x8D, 0x6F, 0x31, 0xB2, 0xEC, 0x0E, 0x50,
    0xAF, 0xF1, 0x13, 0x4D, 0xCE, 0x90, 0x72, 0x2C, 0x6D, 0x33, 0xD1, 0x8F, 0x0C, 0x52, 0xB0, 0xEE,
    0x32, 0x6C, 0x8E, 0xD0, 0x53, 0x0D, 0xEF, 0xB1, 0xF0, 0xAE, 0x4C, 0x12, 0x91, 0xCF, 0x2D, 0x73,
    0xCA, 0x94, 0x76, 0x28, 0xAB, 0xF5, 0x17, 0x49, 0x08, 0x56, 0xB4, 0xEA, 0x69, 0x37, 0xD5, 0x8B,
    0x57, 0x09, 0xEB, 0xB5, 0x36, 0x68, 0x8A, 0xD4, 0x95, 0xCB, 0x29, 0x77, 0xF4, 0xAA, 0x48, 0x16,
    0xE9, 0xB7, 0x55, 0x0B, 0x88, 0xD6, 0x34, 0x6A, 0x2B, 0x75, 0x97, 0xC9, 0x4A, 0x14, 0xF6, 0xA8,
    0x74, 0x2A, 0xC8, 0x96, 0x15, 0x4B, 0xA9, 0xF7, 0xB6, 0xE8, 0x0A, 0x54, 0xD7, 0x89, 0x6B, 0x35
};

uint8_t crc8(uint8_t inData, uint8_t crc)
{
    return pgm_read_byte((CRC8_table + ((inData ^ crc) & 0xFF)));
}

uint8_t crc8(const uint8_t* inPtr, uint8_t len)
{
    uint8_t crc = 0;
    for (uint8_t i = 0; i < len; i++) {
        crc = pgm_read_byte((CRC8_table + ((*inPtr ^ crc) & 0xFF)));
        inPtr += 1;
    }
    return crc;
}

/*
// triggered on falling edge
ISR(INT0_vect) // 1wire asserted
{
    DB_BIT_SET(PORTA, 0);
    TCNT0 = 0; // reset timer
    TIFR0 = _BV(TOV0); // Timer/Counter0 Overflow Flag - set to 1 to clear; // clear Timer/Counter Overflow Flag
    TCCR0B = _BV(CS01); // set Timer/Counter0 prescaler = 8 (1.0 MHz @ 8 MHz system clock); // start timer
    DB_BIT_CLEAR(PORTA, 0);
}
// */

//*
// triggered on falling edge
ISR(INT0_vect, ISR_NAKED) // 1wire asserted
{
    asm volatile(
        // "sbi    %[_PORTA], 0\n" // set PA0

        // "push   r2\n"
        // "in     r2, __SREG__\n"
        // "push   r16\n"
        "out    %[_GPIOR0], r16\n"

        "int0_isr_start:\n"
        // TCNT0 = 0; // reset timer
        "ldi    r16, 0\n"
        "out    %[_TCNT0], r16\n"
        // TIFR0 = _BV(TOV0); // Timer/Counter0 Overflow Flag - set to 1 to clear; // clear Timer/Counter Overflow Flag
        "ldi    r16, %[_TOV0]\n"
        "out    %[_TIFR0], r16\n"
        // TCCR0B = _BV(CS01); // set Timer/Counter0 prescaler = 8 (1.0 MHz @ 8 MHz system clock); // start timer
        "ldi    r16, %[_CS01]\n"
        "out    %[_TCCR0B], r16\n"

        "int0_restore:\n"
        // "pop    r16\n"
        // "out    __SREG__, r2\n"
        // "pop    r2\n"
        "in     r16, %[_GPIOR0]\n"

        // "cbi    %[_PORTA], 0\n" // clear PA0

        "reti\n"
        :
        : [_PORTA] "I"(_SFR_IO_ADDR(PORTA)),
        [_PORTB] "I"(_SFR_IO_ADDR(PORTB)),
        [_GPIOR0] "I"(_SFR_IO_ADDR(GPIOR0)),
        [_TCNT0] "I"(_SFR_IO_ADDR(TCNT0)),
        [_TOV0] "I"(1 << TOV0),
        [_TIFR0] "I"(_SFR_IO_ADDR(TIFR0)),
        [_CS01] "I"(1 << CS01),
        [_TCCR0B] "I"(_SFR_IO_ADDR(TCCR0B)));
}
// */