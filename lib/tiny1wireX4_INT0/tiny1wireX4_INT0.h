#ifndef TINY1WIRE_H
#define TINY1WIRE_H

#include <Arduino.h>
#include "debug.h"
#include "bit_ops.h"

// OW states
#define IDLE 0x00
#define RX 0x10
#define RXROMCMD 0x10
#define RXROMCODE 0x11
#define RXDEVCMD 0x12
#define TX 0x20
#define TXROMCODE 0x21

// OW ROM Commands
#define OW_ROM_READ 0x33
#define OW_ROM_MATCH 0x55
#define OW_ROM_SKIP 0xCC
#define OW_ROM_RESUME 0xA5

// OW pin-related constants
#define OW_PORTX PORTB // output
#define OW_PINX PINB // input
#define OW_DDRX DDRB // direction: 1 = output, 0 = input
#define OW_PIN 2 // PB2 / INT0
#define OW_MASK _BV(OW_PIN)
#define OW_IS_HIGH (OW_PINX & OW_MASK)
#define OW_IS_LOW (!OW_IS_HIGH)
#define OW_ASSERT OW_DDRX |= OW_MASK; DB_BIT_CLEAR(PORTA, 2) // set to output - should already be low
#define OW_RELEASE OW_DDRX &= ~OW_MASK; DB_BIT_SET(PORTA, 2) // set to input to tri-state and allow external pull-up to return line to high

#define TCRESET 255 // 450 µS - anything over 255 will be considered a reset
#define TCERROR 240 // 240 µS

void owSetup(const uint8_t code[7], void (*callback)(uint8_t state, uint8_t* buf));
void owTick();
void owTxBytes(uint8_t* buf, uint8_t len, uint8_t newState = TX);
void owRxBytes(uint8_t len, uint8_t newState);
void owByteRecd();
void owAssert();
void owRelease();
uint8_t crc8(uint8_t inData, uint8_t crc = 0);
uint8_t crc8(const uint8_t* inPtr, uint8_t len);

#endif
