// #pragma GCC optimize ("-O0")
#include "tiny1wireX4_INT0.h"
#include <Arduino.h>
// #include <EEPROM.h>
#include "1wire_node_commands.h"
#include <util/delay.h>

#define I2C_TIMEOUT 1000
#define I2C_PULLUP 1
#define SDA_PORT PORTB
#define SDA_PIN 1 // = B1
#define SCL_PORT PORTB
#define SCL_PIN 0 // = B0
#include "SoftWire.h"

#include "VL53L0X.h"

//                +--\/--+
//            VCC | 1 14 | GND
//      (SCL) PB0 | 2 13 | PA0
//      (SDA) PB1 | 3 12 | PA1
// (RESET/dW) PB3 | 4 11 | PA2 (SERVO CURRENT)
//  (INT0/OW) PB2 | 5 10 | PA3
//      (LED) PA7 | 6  9 | PA4 (SCK)
// (MOSI/SRV) PA6 | 7  8 | PA5 (MISO)
//                +------+

// Arduino ISP
//            GND - 6  5 - Reset
//           MOSI - 4  3 - SCK
//            VCC - 2  1 - MISO

#define LED_ON BIT_SET(LED_PORT, LED_PIN)
#define LED_OFF BIT_CLEAR(LED_PORT, LED_PIN)

#define RXINUM 0x13
#define RXSERVO 0x14

#define VL53L0X_ADDR 0x52

void byteRecd(uint8_t state, uint8_t* buf);

static uint8_t romCode[] = { 0xEE, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }; // Family code: EE
uint8_t data[3];

// SoftWire Wire = SoftWire(); // Instantiated by the library
VL53L0X distanceSensor;
#define RANGING_ERROR 0
#define RANGING_IDLE 1
#define RANGING_CONTINUOUS 2
uint8_t rangingState = RANGING_ERROR;

void setup()
{
    OSCCAL = 0x6E; // User calibration

    // DDxn: 1 = output, 0 = input
    DDRA = 0xFF;
    // DDRB = 0x03;
    // PORTxn: 1 = output high/pull-up enabled, 0 = output low/pull-up disabled
    // PORTA = 0x03;

    BIT_SET(LED_DDR, LED_PIN); // Set LED pin to output
    LED_ON;

    owSetup(romCode, byteRecd);

    // Config Timer 1 for servo using OC1A, TOP = ICR1
    TCCR1A = _BV(COM1A1) | _BV(WGM11); // OC1A Fast PWM
    TCCR1B = _BV(WGM13) | _BV(WGM12); // Fast PWM & prescaler = 0 = stopped
    ICR1 = 20000; // 20000 µS = 20 mS

    // Set up ADC for measuring servo current
    ADMUX = _BV(REFS1) | _BV(MUX1); // Internal 1.1V voltage reference & ADC source PA2
    ADCSRA = _BV(ADATE); // ADC Auto Trigger Enable
    ADCSRB = _BV(ADLAR); // ADC Left Adjust Result & ADC Auto Trigger Source = free running
    DIDR0 = _BV(ADC2D); // Digital Input Disable on ADC2/PA2

    /*
        // Open servo
        OCR1A = ((uint16_t)255 * 5) + 860; // * 4) + 988
        BIT_SET(TCCR1B, CS11); // prescaler = 8

        _delay_ms(500);

        // Turn off servo
        BIT_CLEAR(TCCR1B, CS11); // prescaler = 0 = stopped

        _delay_ms(500);
    */

    Wire.begin();

    if (distanceSensor.init()) {
        rangingState = RANGING_IDLE;
    }

    // distanceSensor.startContinuous();

    LED_OFF;

    sei();
}

// uint16_t ticks = 0;

// #define HIST_SIZE 8
// uint8_t hist[HIST_SIZE];
// uint8_t histPtr = 0;
// uint8_t histCount = 0;
// bool servoClosed = false;

void loop()
{
    // if (rangingState == RANGING_ERROR) {
    //     if (ticks == 0)
    //         LED_ON;
    //     else if (ticks == 0x8000)
    //         LED_OFF;
    //     ticks++;
    // }

    owTick();

    /*
        // static uint8_t count = 0;
        // static uint8_t sum = 0;
        uint16_t mm;
        // mm = distanceSensor.readRangeSingleMillimeters();
        mm = distanceSensor.readRangeContinuousMillimeters();
        // sum += mm;

        // if (mm > 200 && mm < 500) {
        //     LED_ON;
        // } else {
        //     LED_OFF;
        // }

        // if (mm < 256) {
        //     LED_ON;
        //     for(uint8_t i = 0; i < mm; i++)
        //     _delay_ms(2);
        //     LED_OFF;
        //     for(uint8_t i = 0; i < mm; i++)
        //     _delay_ms(2);
        // }

        if (mm > 255)
            hist[histPtr] = 255;
        else
            hist[histPtr] = mm;
        histPtr++;
        if (histPtr == HIST_SIZE)
            histPtr = 0;
        if (histCount < HIST_SIZE)
            histCount++;
        else {
            uint16_t sum = 0;
            for (uint8_t i = 0; i < HIST_SIZE; i++)
                sum += hist[i];
            mm = sum / HIST_SIZE;

            LED_ON;
            for (uint8_t i = 0; i < mm; i++)
                _delay_ms(1);
            LED_OFF;
            for (uint8_t i = 0; i < mm; i++)
                _delay_ms(1);

            if (mm < 36 && !servoClosed) {
                OCR1A = (1 * 5) + 860; // * 4) + 988
                BIT_SET(TCCR1B, CS11); // prescaler = 8
                servoClosed = true;
                for (uint8_t i = 0; i < 250; i++)
                    _delay_ms(20);
                OCR1A = ((uint16_t)255 * 5) + 860; // * 4) + 988
                for (uint8_t i = 0; i < 250; i++)
                    _delay_ms(2);
            } else if (mm > 150 && servoClosed)
                servoClosed = false;
        }
    // */
}

/*
Gripper:

3 A Set Servo
  9 Get Current - 1 byte

5 3 Distance ranging OFF
  6 Distance ranging ON
  9 Get Distance - 1 byte

A 9 Get ID

Common:

C 3 LED OFF
  6 LED ON
*/

void byteRecd(uint8_t state, uint8_t* buf)
{
    uint16_t mm;

    if (state == RXDEVCMD) {
        switch (buf[0]) {
        case OW_GRIPPER_GET_SERVO_CURRENT: // Get current
            data[0] = ADCH;
            data[1] = crc8(data[0]);
            // Send value
            owTxBytes(data, 2);
            break;
        case OW_GRIPPER_SET_SERVO: // Servo setting
            owRxBytes(2, RXSERVO);
            break;
        case OW_GRIPPER_RANGING_OFF: // Distance ranging OFF
            if (rangingState == RANGING_CONTINUOUS) {
                distanceSensor.stopContinuous();
                rangingState = RANGING_IDLE;
            }
            owRxBytes(1, RXDEVCMD);
            break;
        case OW_GRIPPER_RANGING_ON: // Distance ranging ON
            if (rangingState == RANGING_IDLE) {
                distanceSensor.startContinuous();
                rangingState = RANGING_CONTINUOUS;
            }
            owRxBytes(1, RXDEVCMD);
            break;
        case OW_GRIPPER_GET_RANGE: // Get Distance - takes about 1.5 ms
            if (rangingState == RANGING_CONTINUOUS) {
                LED_ON;
                // BIT_SET(PORTA, 0);
                mm = distanceSensor.readRangeContinuousMillimeters();
                // BIT_CLEAR(PORTA, 0);
                if (mm > 255)
                    data[0] = 255;
                else
                    data[0] = mm;
                data[1] = crc8(data[0]);
                LED_OFF;
            } else {
                data[0] = rangingState;
                data[1] = crc8(data[0]);
            }
            // Send value
            owTxBytes(data, 2);
            break;
        case OW_GRIPPER_GET_ID: // Get ID
            data[0] = romCode[0];
            data[1] = romCode[1];
            data[2] = crc8(data, 2);
            // Send value
            owTxBytes(data, 3);
            break;
        case OW_LED_OFF: // turn off LED
            LED_OFF;
            owRxBytes(1, RXDEVCMD);
            break;
        case OW_LED_ON: // turn on LED
            LED_ON;
            owRxBytes(1, RXDEVCMD);
            break;
        default:
            state = IDLE;
        }
    } else if (state == RXSERVO) { // Servo setting
        /*
        1000 µS = open
        2000 µS = closed
        range = 1000 µs
        128 = middle = 1500 µS
        255 = 2008 µS
        1 = 992 µS
        0 = 0
        */
        if (buf[0] == 0) {
            // OCR1A = 0;
            BIT_CLEAR(TCCR1B, CS11); // prescaler = 0 = stopped
            BIT_CLEAR(ADCSRA, ADEN); // Disable ADC
            LED_OFF;
        } else {
            ADCSRA |= _BV(ADEN) | _BV(ADSC); // ADC Enable & ADC Start Conversion
            OCR1A = ((uint16_t)buf[0] * 5) + 860; // * 4) + 988
            BIT_SET(TCCR1B, CS11); // prescaler = 8
            if (buf[0] < 128)
                LED_ON;
            else
                LED_OFF;
        }
        owRxBytes(1, RXDEVCMD);
    }
}
